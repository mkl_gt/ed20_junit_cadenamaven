
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.cuatrovientos.ed.junitcadena.Cadena;
class CadenaTest {

	public Cadena target;

	@BeforeEach
	void setUp() throws Exception {
		target = new Cadena();
	}

	@Test
	void testLongitud() {
		int expected = 4;
		int actual = target.longitud("Hola");
		assertEquals(expected, actual, "Longitud");
	}
	
	@Test
	void testLongitud2() {
		int expected = 0;
		int actual = target.longitud("");
		assertEquals(expected, actual, "Longitud");
	}
	@Test
	void testvocales() {
		int expected = 2;
		int actual = target.vocales("Hola");
		assertEquals(expected, actual, "vocales");
	}
	
	@Test
	void testvocales2() {
		int expected = 0;
		int actual = target.vocales("");
		assertEquals(expected, actual, "vocales");
	}

	@Test
	void testInvertir() {
		String expected = "aloH";
		String actual = target.invertir("Hola");
		assertEquals(expected, actual, "Invertir");
	}
	

	@Test
	void testContar() {
		int expected = 1;
		int actual = target.contarLetra("Hola", "a");
		assertEquals(expected, actual, "Contar Letra");
	}
	@Test
	void testContar2() {
		int expected = 0;
		int actual = target.contarLetra("", "a");
		assertEquals(expected, actual, "Contar Letra");
	}
}
