package org.cuatrovientos.ed.junitcadena;
/**
 * Gesti�n de una cadena de car�cteres
 * 
 * @author Mikel
 *
 */
public class Cadena {

	/**
	 * Funci�n para devolver la longitud de la cadena
	 * 
	 * @param cadena
	 * @return longitud de la cadena
	 */
	public int longitud(String cadena) {
		return cadena.length();
	}

	/**
	 * Funci�n para saber las vocales que tiene la cadena
	 * 
	 * @param cadena
	 * @return numero de vocales que tiene la cadena
	 */
	public int vocales(String cadena) {
		int nVocales = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if (cadena.substring(i, i + 1).equals("a") || cadena.substring(i, i + 1).equals("e")
					|| cadena.substring(i, i + 1).equals("i") || cadena.substring(i, i + 1).equals("o")
					|| cadena.substring(i, i + 1).equals("u")) {
				nVocales = nVocales + 1;
			}
		}
		return nVocales;
	}

	/**
	 * Funci�n para inventir una cadena
	 * 
	 * @param cadena
	 * @return cadena invertida
	 */
	public String invertir(String cadena) {
		String n = "";
		int i = cadena.length();
		do {
			n = n + cadena.substring(i - 1, i);
			i = i - 1;
		} while (i > 0);
		return n;
	}

	/**
	 * Funci�n para saber el n�mero de veces que se repite un caracter
	 * 
	 * @param cadena
	 * @param caracter
	 * @return numero de veces que se repite un caracter
	 */
	public int contarLetra(String cadena, String caracter) {
		int cont = 0;
		for (int i = 0; i < cadena.length(); i++) {
			if ((cadena.substring(i, i + 1)).equals(caracter)) {
				cont += 1;
			}
		}
		return cont;
	}

}
